/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
 $(function(){
    console.log("jQuery Ready");
    
    var app = {
        initialize: function() {
            this.bindEvents();
            this.testzone = {};
            app.lat = 37.789226;
            app.lng = -122.4034491;
        },

        bindEvents: function() {
            document.addEventListener("deviceready", this.onDeviceReady, false);
        },

        onDeviceReady: function() {
        	console.log("Cordova is Ready ...");
        },

        testIfDatabaseExists: function() {
            var exists = window.localStorage.getItem("dbExists");
            console.log("isDatabaseExists" + exists);
            if (exists == null) {
                app.createDatabase();
            } else {
                app.queryDatabase();
            }
        },

        createDatabase: function () {
            app.db.transaction(app.populateDB, app.populateError, app.populateSuccess);
        },

        queryDatabase: function () {
            app.testzone.innerHTML = "Execute Query - get data from database.";
            app.db.transaction(app.queryDB, app.queryError);
        },

        queryDB: function (tx) {
            tx.executeSql('SELECT * FROM pg_users', [], app.querySuccess, app.queryError);
        },

        querySuccess: function (tx, results) {
            var len = results.rows.length;
            var html = "<br />";
            var i = 0;
            for (var i = 0; i < len; i++) {
                html += "ID: " + results.rows.item(i).id + "<br />";
                html += "Firstname: " + results.rows.item(i).firstname + "<br />";
                html += "Lastname: " + results.rows.item(i).lastname + "<br />";
                html += "Age: " + results.rows.item(i).age + "<br />";
                app.testzone.innerHTML += html;
                html = "<br />";
            }
        },

        queryError: function (err) {
            app.testzone.innerHTML = "ERROR: " + err.code + "<br />";
        },

        populateDB: function (tx) {
            tx.executeSql('DROP TABLE IF EXISTS pg_users');
            tx.executeSql('CREATE TABLE IF NOT EXISTS pg_users (id unique, firstname, lastname, age)');
            tx.executeSql('INSERT INTO pg_users (id, firstname, lastname, age) VALUES (1, "Sanja", "Herceg", 9)');
            tx.executeSql('INSERT INTO pg_users (id, firstname, lastname, age) VALUES (2, "Predrag", "Jovanovic", 25)');
        },

        populateError: function (error) {
            app.testzone.innerHTML = "Database Error: " + error.code;
        },

        populateSuccess: function () {
            window.localStorage.setItem("dbExists", 1);
            app.testzone.innerHTML = "Database populated successfully!";
        },

        testForNumber: function () {
            app.testzone.addEventListener('click', app.setNewNumber, false);
            var number = window.localStorage.getItem("bigNumber");
            if(number != null){
                app.testzone.innerHTML = "Your last random number was :" + number;
            }else{
                app.testzone.innerHTML = "You have never generate number before!";
            }
        },
        setNewNumber: function(evt) {
            var number = Math.floor( (Math.random()*1000000)+1 );
            app.testzone.innerHTML = "Your new Big Number is: " + number;
            window.localStorage.setItem("bigNumber", number); 
        },
        onGeoSuccess: function(position) {
            app.testzone.innerHTML = "Position latitude :" + position.coorde.latitude + "<br />" + 
                                     "Position longitude :" + position.coorde.longitude + "<br />";

        },
        onGeoError: function(error) {
            alert("Error code: " + error.code + "\n" + "Error message: " + error.message + "\n");
        },
        getPlace: function() {
            $.getJSON("https://maps.googleapis.com/maps/api/geocode/json?latlng=" + app.lat + "," + app.lng + "&sensor=false", 
                        function(data){
                            app.testzone.innerHTML = data.results[0].formatted_address;
                        }
                     );
        },
        getDeviceInfo: function() {
        	app.testzone.innerHTML += "Device properties: name - " + device.model + "<br />" +
        							 "Cordova - " + device.cordova + "<br />" +
        							 "Platform - " + device.platform + "<br />" +
        							 "UUID - " + device.uuid + "<br />" +
        							 "Device Version - " + device.version + "<br />";
        },
        getConnectionInfo: function() {
        	var networkstate = navigator.connection.type;
        	var states = {};
        	states[Connection.UNKNOWN] = "UNKNOWN";
        	states[Connection.ETHERNET] = "ETHERNET";
        	states[Connection.WIFI] = "WIFI";
        	states[Connection.CELL_2G] = "CELL_2G";
        	states[Connection.CELL_3G] = "CELL_3G";
        	states[Connection.CELL_4G] = "CELL_4G";
        	states[Connection.CELL] = "CELL";
        	states[Connection.NONE] = "NONE";
        	
        	app.testzone.innerHTML += "Connetion State:" + states[networkstate];
        },
        runAlertNotif: function() {
        	navigator.notification.alert(
        			"Alert message!",
        			app.alertDismissed,
        			"Alert Box",
        			"Next"
        	);
        },
        alertDismissed: function() {
        	navigator.notification.confirm(
        			"Do you want beeps and vibrations?",
        			app.onConfirm,
        			"Confirm Box",
        			["Yes", "No"]
        	);
        },
        onConfirm: function(index) {
        	if(index == 2) {
        		alert("You selected NO");
        	} else {
        		navigator.notification.beep(2);
        		navigator.notification.vibrate(2500);
        	}
        }
    };
    
    app.initialize();
});